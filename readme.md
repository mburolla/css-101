# CSS 101
Intro to CSS notes based on [this](https://youtu.be/yfoY53QXEnI) YouTube video.

# EM vs EX vs PX
- em : the font-size of the relevant font
- ex : the x-height of the relevant font
- px : pixels, relative to the viewing device

# Class vs ID
- Assign IDs to things that are unique
- CSS classes are designed to be resused

# CSS Selector
- A CSS selector selects the HTML element(s) you want to style.
- https://www.w3schools.com/css/css_selectors.asp

# CSS Box Model
<img src="./docs/css-box-model.PNG" />

```
p {
    margin: 5px 10px 5px 10px; // TOP, RIGHT, BOTTOM, LEFT
}

p {
    margin: 5px 10px; // 5 for the TOP & BOTTOM, 10 LEFT & RIGHT
}

p {
    margin: 5px; // 5 PX for TOP, RIGHT, BOTTOM, LEFT
}
```

# Positioning
- Static
- Relative
- Absolute
- Fixed
- Initial
- Inherit

# Pseudo  Class

```
.my-list li:first-child { /* psuedo class */
    background: red;
}   

.my-list li:last-child {
    background: blue;
}   

.my-list li:nth-child(5) {
    background: yellow;
}  
```